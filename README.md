# Github Releases Watcher

A script that watches for new releases on GitHub and (only) triggers a gitlab pipeline (for now).

---

### Gitlab

In your [Preferences](https://gitlab.com/-/profile/personal_access_tokens) you can generate Access tokens. The only important thing to know is that you need the `api` scope. Then you put the token you recieved from gitlab in the environment variables.

### Github

In the [Developer settings](https://github.com/settings/tokens) in the settings for you account. you can generate Access tokens. Generate a new (classic) token. Here again it doesn't really matter what you choose as long as you enable `public_repo` under the scopes section. Then you also put this token in the environment variables.

### Generating Gitlab trigger tokens

For now you just have the fill in the `gitlabProjectId` if you already put the `GITLAB_TOKEN` in your environment (if not just fill it into the field inside the field like `gitlabProjectId`). Then you run the script, it will spit out a token, you can then use this one in `config.json`. So the script can trigger the pipeline.

## Todo

- [ ] Implement notifications
  - [ ] SMTP
  - [ ] Webhooks
- [ ] Clean up the code
- [ ] Implement other platforms (for watching)?
