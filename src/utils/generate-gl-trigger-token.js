require('dotenv').config()
const axios = require('axios');
const FormData = require('form-data');
let data = new FormData();
data.append('description', 'Trigger Token for Github releases watcher');

let gitlabProjectId = ""
let GITLAB_TOKEN = "" | process.env.GITLAB_TOKEN

let config = {
  method: 'post',
maxBodyLength: Infinity,
  url: `https://gitlab.com/api/v4/projects/${gitlabProjectId}/triggers`,
  headers: { 
    'PRIVATE-TOKEN': GITLAB_TOKEN, 
    ...data.getHeaders()
  },
  data : data
};

axios(config)
.then(function (response) {
  console.log("-----")
  console.log("Token: " + response.data.token)
  console.log("-----")
  console.debug(response.data);
})
.catch(function (error) {
  console.log(error);
});
