import dotenv from "dotenv";
import axios from "axios";
import fs from "fs";

const versionRegex = /(\d+\.)?(\d+\.)?(\*|\d+)/g;

dotenv.config();

let interval = 15; // in minutes

const LATEST_FILE: string = (process.env.LATEST_FILE = "latest.json");
const CONFIG_FILE: string = (process.env.CONFIG_FILE = "config.json");

interface Config {
  githubProjectId: string;
  gitlabProjectId: string;
  gitlabTriggerToken: string;
  branch: string;
}

async function readJsonFile(path: string) {
  let original = await fs.readFileSync((process.env.LATEST_FILE = path));
  return JSON.parse(original.toString());
}

if (process.env.INTERVAL != null) {
  interval = parseInt(process.env.INTERVAL);
}

async function getLatestTag(githubProjectId: string) {
  return await axios(`https://api.github.com/repos/${githubProjectId}/tags`, {
    headers: {
      Accept: "application/vnd.github+json",
      Autorizatoin: "Bearer" + process.env.GITHUB_TOKEN,
      "X-GitHub-Api-Version": "2022-11-28",
    },
  }).then(async (response) => {
    return response.data[0].name;
  });
}

async function triggerPipeline(
  tag: string,
  project_id: string,
  gitlabTriggerToken: string,
  branch: string = "main"
) {
  let encodedId = encodeURIComponent(project_id);
  let version: RegExpMatchArray | null | string = tag.match(versionRegex);
  if (version == null) {
    return console.error("Version not found");
  }

  version = version[0];

  try {
    let data = await axios(
      `https://gitlab.com/api/v4/projects/${encodedId}/trigger/pipeline`,
      {
        method: "POST",
        headers: {
          "PRIVATE-TOKEN": process.env.GITLAB_TOKEN,
        },
        params: {
          ref: branch,
          token: gitlabTriggerToken,
        },
        data: {
          variables: {
            CADDY_VERSION: version,
            PUSH_LATEST: "true",
          },
        },
      }
    );

    console.log(data.data);
  } catch (error) {
    console.error(error);
  }
}
async function main(
  githubProjectId: string,
  gitlabProjectId: string,
  gitlabTriggerToken: string,
  branch: string
) {
  let lastTag = await getLatestTag(LATEST_FILE)[githubProjectId];
  let latest_tag = await getLatestTag(githubProjectId);

  if (lastTag != latest_tag) {
    lastTag[githubProjectId] = latest_tag;
    await fs.writeFileSync(LATEST_FILE, JSON.stringify(lastTag));

    await triggerPipeline(
      latest_tag,
      gitlabProjectId,
      gitlabTriggerToken,
      branch
    );
  }
}

(async () => {
  setInterval(async () => {
    let config: Config[] = await readJsonFile(CONFIG_FILE);
    console.log(config);
    config.forEach(async (config) => {
      await main(
        config.githubProjectId,
        config.gitlabProjectId,
        config.gitlabTriggerToken,
        config.branch
      );
    });
  }, interval * 1000 * 60); // minutes to milliseconds (which is the unit of setInterval)
})();
